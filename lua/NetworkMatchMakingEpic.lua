Hooks:PostHook(NetworkMatchMakingEPIC, "join_server", "LobbyInspect:join_server", function(self, room_id, ...)
	if LobbyInspect and room_id then
		LobbyInspect.room_id = room_id
	end
end)

local SearchLobbyOriginal = NetworkMatchMakingEPIC.search_lobby
function NetworkMatchMakingEPIC:search_lobby(friends_only, ...)
	if LobbyInspect.allow_connection == true then
		return
	else
		SearchLobbyOriginal(self, friends_only, ...)
	end
end