if not _G.LobbyInspect then
	_G.LobbyInspect = _G.LobbyInspect or {}
	LobbyInspect.Name = "Lobby Inspect"
	LobbyInspect._path = ModPath
	LobbyInspect._data_path = SavePath .. "LobbyInspect.txt"
	LobbyInspect.settings = {
		mode = 2,
		hours = true,
		achievements = true,
		vac = true,
		steam_overlay = true,
		steamrep_overlay = true,
		friends = false,
		language = 1,
	}
	LobbyInspect.PlayerName = nil
	LobbyInspect.host_id = nil
	LobbyInspect.room_id = nil
	LobbyInspect.allow_connection = false
	LobbyInspect.Init = {false, false, false}
end

function LobbyInspect:InitStuff()
	dohttpreq("http://steamcommunity.com/",
	function(page)
		local _, start = string.find(page, '<span>View mobile website</span>')
		if start then
			LobbyInspect.Init[1] = true
		end
	end)
end
LobbyInspect:InitStuff()
function LobbyInspect:Load()
	local file = io.open(self._data_path, "r")
	if file then
		for k, v in pairs(json.decode(file:read("*all")) or {}) do
			self.settings[k] = v
		end
		file:close()
	end
end

function LobbyInspect:Save()
	local file = io.open(self._data_path, "w+")
	if file then
		file:write(json.encode(self.settings))
		file:close()
	end
end

Hooks:Add("LocalizationManagerPostInit", "LobbyInspect:LocalizationManager", function(loc)
	LobbyInspect:Load()
	local mpath = LobbyInspect._path  .. "loc/"
	loc:load_localization_file(mpath.."initial.json")
	local path
	if LobbyInspect.settings.language == 1 then
		path="english.json"
	elseif LobbyInspect.settings.language == 2 then
		path="french.json"
	elseif LobbyInspect.settings.language == 3 then
		path="german.json"
	elseif LobbyInspect.settings.language == 4 then
		path="italian.json"
	elseif LobbyInspect.settings.language == 5 then
		path="russian.json"
	elseif LobbyInspect.settings.language == 6 then
		path="turkish.json"
	elseif LobbyInspect.settings.language == 7 then
		path="spanish.json"
	end
	loc:load_localization_file(mpath..path)
end)

Hooks:Add("MenuManagerInitialize", "LobbyInspect:MenuManagerInitialize", function(menu_manager)
	MenuCallbackHandler.lobby_inspect_type_callback = function(this, item)
		LobbyInspect.settings.mode = item:value()
	end
	MenuCallbackHandler.lobby_inspect_language_callback = function(this, item)
		LobbyInspect.settings.language = item:value()
	end
	MenuCallbackHandler.lobby_inspect_friends_callback = function(this, item)
		LobbyInspect.settings.friends = (item:value() == "on" and true or false)
	end
	MenuCallbackHandler.lobby_inspect_hours_callback = function(this, item)
		LobbyInspect.settings.hours = (item:value() == "on" and true or false)
	end
	MenuCallbackHandler.lobby_inspect_achievements_callback = function(this, item)
		LobbyInspect.settings.achievements = (item:value() == "on" and true or false)
	end
	MenuCallbackHandler.lobby_inspect_vac_callback = function(this, item)
		LobbyInspect.settings.vac = (item:value() == "on" and true or false)
	end
	MenuCallbackHandler.lobby_inspect_steam_overlay_callback = function(this, item)
		LobbyInspect.settings.steam_overlay = (item:value() == "on" and true or false)
	end
	MenuCallbackHandler.lobby_inspect_steamrep_overlay_callback = function(this, item)
		LobbyInspect.settings.steamrep_overlay = (item:value() == "on" and true or false)
	end
	MenuCallbackHandler.lobby_inspect_menu = function(this, item)
		LobbyInspect:Save()
	end
	LobbyInspect:Load()
	MenuHelper:LoadFromJsonFile(LobbyInspect._path .. "menu/options.json", LobbyInspect, LobbyInspect.settings)
end)

function ContinueConnecting()
	managers.network.matchmake:join_server(LobbyInspect.room_id)
	LobbyInspect.allow_connection = true
	DelayedCalls:Add("LobbyInspect:Disallow_connections", 5, function()
		LobbyInspect.allow_connection = false
	end)
end

function SteamFriend(user_id)
		local afriend = false
		if Steam:logged_on() then
			for _, friend in ipairs(Steam:friends() or {}) do
				if friend:id() == user_id then
					afriend = true
					break
				end
			end
		end
		return afriend
	end

function DataCollect()
	managers.menu:show_joining_lobby_dialog()
	DelayedCalls:Add("LobbyInspect:Force_close_stale", 10, function()
		if not Utils:IsInGameState() then
			managers.system_menu:close("join_server")
		end
	end)
	local hours = ""
	local achievements = ""
	local vac = managers.localization:text("lobby_inspect_clean")
	if LobbyInspect.Init[1] == false then
		hours = "down"
	end
	if LobbyInspect.Init[1] == true then
		dohttpreq("http://steamcommunity.com/profiles/".. LobbyInspect.host_id .. "/?l=english",
		function(page)
			if LobbyInspect.settings.hours == true then
				local _, hours_start = string.find(page, '<div class="game_info_details">')
				if hours_start then
					local hours_ends = string.find(page, '<div class="game_name"><a', hours_start)
					if hours_ends then
						hours = (string.sub(page, hours_start, hours_ends))
						hours = string.gsub(hours, "	", "")
						hours = string.gsub(hours, "hrs on record<br>", "")
						hours = string.gsub(hours, "<", "")
						hours = string.gsub(hours, ">", "")
						hours = string.split(hours, "\n")
						hours = hours[2]
						hours = string.gsub(hours, ",", "")
						hours = (math.floor((hours + 1/2)/1) * 1)
						hours = tonumber(hours)
						if hours ~= nil then
							hours = (math.floor((hours + 1/2)/1) * 1)
						end
					end
				end
			end
			if LobbyInspect.settings.vac == true then
				local _, vac_exists = string.find(page, '<div class="profile_ban_status">')
				if vac_exists then
					vac = managers.localization:text("lobby_inspect_warning")
				end
			end
			if hours ~= "" then
				if LobbyInspect.settings.achievements == true then
					local _, ach_start = string.find(page, '<h2>Recent Activity</h2>')
					if ach_start then
						local ach_ends = string.find(page, '<span>View mobile website</span>', ach_start)
						if ach_ends then
							local page1 = (string.sub(page, ach_start, ach_ends))
							if page1 then
								local _, ach1_start = string.find(page1, '<span class="ellipsis">')
								if ach1_start then
									local ach1_ends = string.find(page1, '<div class="achievement_progress_bar_ctn">', ach1_start)
									if ach1_ends then
										achievements = (string.sub(page1, ach1_start, ach1_ends))
										achievements = string.split(achievements, " of")
										local achievements2 = string.sub(achievements[2], 2, 4)
										achievements = achievements[1]
										achievements = string.gsub(achievements, ">", "")
										achievements = achievements .. " / " .. achievements2
										--log("achievements" .. achievements)
									end
								end
							end
						end
					end
				end
			end
			PrepareData(hours, vac, achievements)
		end)
	else
		PrepareData(hours, vac, achievements)
	end
end

function PrepareData(hours, vac, achievements)
	if hours == "" then
		hours = "\n" .. managers.localization:text("lobby_inspect_playtime") .. ": " .. managers.localization:text("lobby_inspect_hidden")
	elseif hours == nil or hours == "nil" then
		hours = "\n" .. managers.localization:text("lobby_inspect_playtime") .. ": " .. managers.localization:text("lobby_inspect_failed")
	elseif hours == "down" then
		hours = "\n" .. managers.localization:text("lobby_inspect_steam_down") .. "!"
		vac = ""
	else
		hours = "\n" .. managers.localization:text("lobby_inspect_playtime") .. ": " .. tostring(hours) ..  " " .. managers.localization:text("lobby_inspect_hours")
	end
	if achievements == "" then
		achievements = ""
	else
		achievements = "\n" .. managers.localization:text("lobby_inspect_achievements_title") .. ": " .. achievements
	end
	local message = LobbyInspect.PlayerName .. "\n"
	if NoobJoin then
		if NoobJoin:Is_From_Blacklist(LobbyInspect.host_id) == true then
			message = message .. "\n" .. managers.localization:text("lobby_inspect_is_from_blacklist")
		end
	end
	if LobbyInspect.settings.hours == true then
		message = message .. hours
	end
	if LobbyInspect.settings.achievements == true then
		message = message .. achievements
	end
	if LobbyInspect.settings.vac == true then
		message = message .. " \n" .. managers.localization:text("lobby_inspect_vac") .. ": " .. vac
	end
	ContinueInspecting(message)
end

function DoHttpLookup(data)
	if data[1] == "steam" then
		Steam:overlay_activate("url", "http://steamcommunity.com/profiles/" .. LobbyInspect.host_id)
	elseif data[1] == "steamrep" then
		Steam:overlay_activate("url", "http://steamrep.com/profiles/" .. LobbyInspect.host_id)
	end
	ContinueInspecting(data[2])
end

function NGBTO_Add_Cheater(data)
	local menu_options = {}

	menu_options[#menu_options+1] = {text = managers.localization:text("lobby_inspect_ban"), data = nil, callback = NGBTO_Add_CheaterNow}
	menu_options[#menu_options+1] = {text = "", data = nil, callback = nil}
	menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_cancel"), data = data[2], callback = ContinueInspecting}

	local menu = QuickMenu:new(managers.localization:text("lobby_inspect_options"), managers.localization:text("lobby_inspect_ban_confirm") .. " " .. LobbyInspect.PlayerName .. "?", menu_options)
	menu:Show()
end

function NGBTO_Add_CheaterNow()
	local menu_options = {}

	menu_options[#menu_options+1] = {text = managers.localization:text("lobby_inspect_okay"), is_cancel_button = true}

	local menu = QuickMenu:new(managers.localization:text("lobby_inspect_options"), LobbyInspect.PlayerName .. " " .. managers.localization:text("lobby_inspect_banned") .. ".", menu_options)
	NoobJoin:Add_Cheater(LobbyInspect.host_id, LobbyInspect.PlayerName, "Manual ban")
	menu:Show()
end

function ContinueInspecting(message)
	local menu_options = {}
	local data_package = {{"steam", message}, {"steamrep", message}}

	menu_options[#menu_options+1] = {text = managers.localization:text("lobby_inspect_refresh"), data = nil, callback = DataCollect}
	menu_options[#menu_options+1] = {text = "", data = nil, callback = nil}
	if LobbyInspect.settings.steam_overlay == true then
		menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_steam_overlay_title"), data = data_package[1], callback = DoHttpLookup}
	end
	if LobbyInspect.settings.steamrep_overlay == true then
		menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_steamrep_overlay_title"), data = data_package[2], callback = DoHttpLookup}
	end
	menu_options[#menu_options+1] = {text = "", data = nil, callback = nil}
	if NoobJoin then -- If you have NGBTO you can ban people! For those lovely NGBTO users, shoutout to everyone in my friendlist, I love you guys. ;)
		menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_ban"), data = data_package[1], callback = NGBTO_Add_Cheater}
		menu_options[#menu_options+1] = {text = "", data = nil, callback = nil}
	end
	menu_options[#menu_options+1] = {text = managers.localization:text("lobby_inspect_cancel"), is_cancel_button = true}
	menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_connect"), data = nil, callback = ContinueConnecting}

	local menu = QuickMenu:new(managers.localization:text("lobby_inspect_options"), message, menu_options)
	managers.system_menu:close("join_server")
	menu:Show()
end

function Midstop()
	local menu_options = {}

	menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_inspect"), data = nil, callback = DataCollect}
	menu_options[#menu_options+1] = {text = "", data = nil, callback = nil}
	menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_cancel"), is_cancel_button = true}
	menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_connect"), data = nil, callback = ContinueConnecting}

	local menu = QuickMenu:new(managers.localization:text("lobby_inspect_options"), message, menu_options, tweak_data.chat_colors[1])
	menu:Show()
end